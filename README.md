INTRODUCTION
------------

The Paragraph Panes provides a ctools plugin which allows you to add paragraph
items to panels in Page Manager.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/paragraph_panes

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/paragraph_panes

REQUIREMENTS
------------

This module requires the following modules:

 * Paragraphs (https://drupal.org/project/paragraphs)
 * Panels (https://drupal.org/project/panels)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
   
CONFIGURATION
-------------

The module has no menu or modifiable settings. It will add a category "Paragraph
Panes" to the Page Manager content selection interface. In this category you
will find all your Paragraph Bundles.

MAINTAINERS
-----------

Current maintainers:
 * Sneakyvv - https://drupal.org/user/1823074
